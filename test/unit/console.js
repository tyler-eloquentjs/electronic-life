var reporter = require("../../reporters/console.js"),
    config = require("../../game/config.js");

describe("Console Reporter", ()=>{
  it("outputs to console", ()=>{
    reporter.recordState(config.setConfig([
        "# # #".split(""),
        "# # #".split(""),
        "# #O#".split("")],
    {
      "O": {"type": "randomWalker", "inertia": 3},
      "#": {"type": "tree"}
    },
    [3, 5]));
  });
});
