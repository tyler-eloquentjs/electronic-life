const assert = require("assert"),
config = require("../../game/config.js"),
utils = require("../../game/utils"),
actions = require("../../game/actions.js"),
legend = {
  "O": {
    "type": "randomWalker",
    "energy": 100,
    "meat": true,
    "inertia": 3,
    "actions": ["randomMove", "fatigue", "eat"]
  },
  "B": {
    "type": "berries",
    "energy": 50,
    "edible": true,
    "growth": 2,
    "actions": ["grow"]
  },
  "#": {
    "type": "tree",
    "actions": []
  },
  "I": {
    "type": "linearBouncer",
    "energy": 100,
    "carnivore": true,
    "inertia": 1,
    "actions": ["linearMove", "fatigue", "eat"]
  }
};

describe("Actions", ()=>{
  it("can run action functions on an entity", ()=>{
    var entities = [
      {type:"consumer", posn: [3, 2], actions: ["eat"], growth: 3, energy: 5},
      {type:"food", edible: true, posn: [2, 2], energy: 3},
    ],
    result = actions.act(entities, 0);

    assert.equal(result[0][0].energy, 8);
  });
  it("can move an entity to any open space", ()=>{
    var entities = config.setConfig([
      "# ###".split(""),
      "## ##".split(""),
      "#O  #".split(""),
      "## ##".split(""),
      "#  ##".split("")
    ], legend),
    openSpaces = [
      [2, 2], [1, 2], [3, 2]
    ],
    destinationSpaces = [],
    newEntities;

    while (destinationSpaces.length !== openSpaces.length) {
      newEntities = actions.act(entities, 9)[0];
      destn = newEntities[9].posn;

      if (!openSpaces.some(validateDestination)) {
        throw new Error("invalid destination found");
      }

      if (destinationSpaces.indexOf(destn.join()) === -1) {
        destinationSpaces.push(destn.join());
      }
    }

    function validateDestination(posn) {
      return destn[0] === posn[0] && destn[1] === posn[1];
    }
  });

  it("can move an entity in a straight line starting from no movement", ()=>{
    var entities = config.setConfig([
      "#I###".split(""),
      "## ##".split(""),
      "#   #".split(""),
      "## ##".split(""),
      "#  ##".split("")
    ], legend),
    result;

    result = actions.act(entities, 1)[0];
    assert.deepEqual(result[1].posn, [1, 2]);
  });

  it("can move an entity in a straight line from a previous position", ()=>{
    var entities = config.setConfig([
      "#####".split(""),
      "#  ##".split(""),
      "#I ##".split(""),
      "#  ##".split(""),
      "#####".split(""),
    ], legend),
    result;

    entities[9].prevPosn = [1, 1];

    result = actions.act(entities, 9);

    assert.deepEqual(result[0][9].posn, [3, 1]);
  });

  it("can transfer energy through eating veggies", ()=>{
    var entities = config.setConfig([
      "#####".split(""),
      "#BI##".split(""),
      "#  ##".split(""),
      "#  ##".split(""),
      "#####".split(""),
    ], legend),
    result;

    result = actions.act(entities, 7);
    assert.equal(result[0][7].energy, 100);
    assert.equal(result[0][6].energy, 49);
  });

  it("can transfer energy through eating meat", ()=>{
    var entities = config.setConfig([
      "#####".split(""),
      "#OI##".split(""),
      "#  ##".split(""),
      "#  ##".split(""),
      "#####".split(""),
    ], legend),
    result;

    result = actions.act(entities, 7);
    assert.equal(result[0][7].energy, 100);
    assert.equal(result[0][6].energy, 99);
  });

  it("can acquire energy through growth", ()=>{
    var entities = config.setConfig([
      "#####".split(""),
      "#B ##".split(""),
      "#  ##".split(""),
      "#  ##".split(""),
      "#####".split(""),
    ], legend),
    result;

    result = actions.act(entities, 6);
    assert.equal(result[0][6].energy, 52);
  });
});

