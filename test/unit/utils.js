const assert = require("assert"),
config = require("../../game/config.js"),
utils = require("../../game/utils");

describe("Utils", ()=>{
  it("validates an entity exists", ()=>{
    assert.throws(()=>{utils.validateEntity();}, /ether/);
  });

  it("validates entity position", ()=>{
    assert.throws(()=>{utils.validateEntity({posn: 2});}, /position/);
  });

  it("returns empty spaces for a posn", ()=>{
    var entities = config.setConfig([
      "#O  #".split(""),
      "## ##".split(""),
      "#O ##".split("")
    ],
    {
      "O": {"type": "randomWalker", "inertia": 3},
      "#": {"type": "tree"}
    });

    assert.deepEqual(utils.emptySpaces(entities, 1), [[0, 2], [1, 2]]);
    assert.deepEqual(utils.emptySpaces(entities, 8), [[1, 2], [2, 2]]);
  });
});
