var assert = require("assert"),
config = require("../../game/config.js");

describe("Config", ()=>{
  it("receives a configuration of map, legend, and size", ()=>{
    config.setConfig([
        "###".split(""),
        "###".split(""),
        "###".split("")
    ],
    {
      "#": {}
    },
    [5, 5]);
  });

  it("validates a gamemap", ()=>{
    config.parse([
      "# ###".split(""),
      "## ##".split(""),
      "#O  #".split(""),
      "## ##".split(""),
      "#  ##".split("")
    ],
    {
      "O": {"type": "randomWalker", "inertia": 3},
      "#": {"type": "tree"}
    },
    [5, 5]);
  });

  it("throws if each row is not an array", ()=>{
    assert.throws(()=>{
      config.parse(
      [
      "# ###".split(""),
      5,
      "#O  #".split(""),
      "## ##".split(""),
      "#  ##".split("")
    ],
    {
      "O": {"type": "randomWalker", "inertia": 3},
      "#": {"type": "tree"}
    },
    [5, 5]);
    });
  });

  it("throws if each row array is not made up of strings", ()=>{
    assert.throws(()=>{
      config.parse(
      [
      ["#","#","#",5,"#"],
      "#O  #".split(""),
      "## ##".split(""),
      "#  ##".split("")
    ],
    {
      "O": {"type": "randomWalker", "inertia": 3},
      "#": {"type": "tree"}
    },
    [5, 5]);
    });
  });

  it("throws if undefined action is indicated", ()=>{
    assert.throws(()=>{
      config.parse(
      [
      ["#","#","#",5,"#"],
      "#O  #".split(""),
      "## ##".split(""),
      "#  ##".split("")
      ],
      {
        "O": {"type": "randomWalker", "inertia": 3},
        "#": {"type": "tree", "actions": ["nonexistentaction"]}
      });
    }, /invalid action/);
  });

  it("sets size and initial entities on config", ()=>{
    var entities = config.setConfig([
      "#   #".split(""),
      "     ".split(""),
      " O   ".split(""),
      "     ".split(""),
      "    #".split("")
    ],
    {
      "O": {"type": "randomWalker", "inertia": 3},
      "#": {"type": "tree"}
    });

    assert.deepEqual(config.size(), [5, 5]);
    assert.ok(entities.some((entity)=>{
      return entity.type === "tree" && entity.posn[0] === 0 && entity.posn[1] === 0;
    }));
    assert.ok(entities.some((entity)=>{
      return entity.type === "tree" && entity.posn[0] === 0 && entity.posn[1] === 4;
    }));
    assert.ok(entities.some((entity)=>{
      return entity.type === "tree" && entity.posn[0] === 4 && entity.posn[1] === 4;
    }));
    assert.ok(entities.some((entity)=>{
      return entity.type === "randomWalker" && entity.posn[0] === 2 && entity.posn[1] === 1;
    }));
    assert.equal(entities.length, 4);
  });


});
