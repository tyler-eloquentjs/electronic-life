var config = require("../game/config.js");
var clearScreen = "\033c";

var runningUnderTest = process.argv.some(arg=>{
  return arg.indexOf("mocha") > -1;
}),
characters = {
  "randomWalker": "O",
  "berries": "B",
  "tree": "#",
  "linearBouncer": "I"
};

if (!runningUnderTest) {console.log(clearScreen);}

module.exports = {
  init(){},
  recordState(entities) {
    var mapState = Array(config.size()[0]).fill(" ").map((row)=>{
      return Array(config.size()[1]).fill(" ");
    });

    if (!runningUnderTest) {
      console.log(clearScreen);
    }

    entities.forEach((entity)=>{
      var row = entity.posn[0], col = entity.posn[1];
      if (!mapState[row]) {mapState[row] = [];}

      mapState[row][col] = characters[entity.type];
    });

    mapState.forEach((row)=>{
      row.forEach((character)=>{
        if (!character) {character = " ";}
      });
      console.log(row.join(""));
    });
  }
};
