const graphics = require("../graphics/graphics40px.js"),
config = require("../game/config.js"),
px = graphics.pixelDimension,
entityImages = {
  "tree": graphics.tree,
  "grass": graphics.grass,
  "randomWalker": graphics.randomWalker,
  "linearBouncer": graphics.linearBouncer,
  "berries": graphics.berries
},
canvas = document.createElement("canvas"),
ctx = canvas.getContext("2d");

function clear() {
  for (var i = 0, j = canvas.height; i < j; i += px) {
    for (var w = 0, x = canvas.width; w < x; w += px) {
      ctx.drawImage(graphics.grass, w, i);
    }
  }
}

module.exports = {
  init() {
    canvas.width = config.size()[1] * px;
    canvas.height = config.size()[0] * px;
    document.body.appendChild(canvas);
  },
  recordState(entities) {
    clear();
    entities.forEach((entity)=>{
      x = entity.posn[1] * px,
      y = entity.posn[0] * px;

      ctx.drawImage(entityImages[entity.type], x, y);

      if (!entity.energy || entity.energy <= 0) {return;}
      ctx.fillStyle = "rgba(0, 0, 200, 0.7)";
      ctx.fillRect(x, y + (px / 10), px * entity.energy / 100, px / 10);
      ctx.fillText(entity.energy, x, y);
    });
  }
};
