const utils = require("./utils");

const actions = {
  fatigue: require("./actions/fatigue.js"),
  randomMove: require("./actions/random-move.js"),
  linearMove: require("./actions/linear-move.js"),
  eat: require("./actions/eat.js"),
  grow: require("./actions/grow.js")
};

module.exports = {
  act(entities, idx) {
    return entities[idx].actions
    .map((action)=>{return actions[action];})
    .reduce((entityState, actFunction)=>{
      return actFunction(entityState);
    }, [entities, idx]);
  },
  actions
};
