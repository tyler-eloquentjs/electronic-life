const actions = require("./actions.js"),
config = require("./config.js"),
utils = require("./utils"),
reporters = [
  typeof window === "undefined" ? require("../reporters/console.js") : require("../reporters/canvas.js")
];

var entities = config.useDefaultConfig();

reporters.forEach((reporter)=>{reporter.init(entities); reporter.recordState(entities);});

setInterval(()=>{
  entities.forEach((entity, idx)=>{
    utils.validateEntity(entities[idx]);
    entities = actions.act(entities, idx)[0];
  });

  reporters.forEach(reporter=>{reporter.recordState(entities);});
}, 300);
