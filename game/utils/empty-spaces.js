const config = require("../config.js");

module.exports = function emptySpaces(entities, idx) {
  var entityPosn = entities[idx].posn, emptyPosns = [], rowOffset, colOffset;

  function notObstructing(entity) {
    return entity.posn[0] !== entityPosn[0] + rowOffset || 
    entity.posn[1] !== entityPosn[1] + colOffset;
  }

  for (rowOffset = -1; rowOffset < 2; rowOffset += 1) {
    for (colOffset = -1; colOffset < 2; colOffset += 1) {
      if (rowOffset === 0 && colOffset === 0) {continue;}

      if (entityPosn[0] + rowOffset < 0 ||
      entityPosn[1] + colOffset < 0) {continue;}

      if (entityPosn[0] + rowOffset > config.size()[0] - 1 ||
      entityPosn[1] + colOffset > config.size()[1] - 1) {continue;}

      if (entities.every(notObstructing)) {
        emptyPosns.push([entityPosn[0] + rowOffset, entityPosn[1] + colOffset]);
      }
    }
  }

  return emptyPosns;
};
