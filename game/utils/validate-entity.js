module.exports = function validateEntity(entity) {
  if (!entity) {
    throw new Error("cannot move the ether");
  }
  if (!Array.isArray(entity.posn)) {
    throw new Error("position should be an array");
  }
};
