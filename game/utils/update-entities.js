module.exports = function updateEntities(entities, changes) {
  if (!changes) {return entities;}

  var result = Array.from(Array(entities.length), (a, i)=>{
    return Object.assign({}, entities[i]);
  });

  changes.forEach((changeItem)=>{
    result[changeItem.idx] = Object.assign(result[changeItem.idx], changeItem.change);
  });
  return result;
};
