module.exports = {
  validateEntity: require("./validate-entity.js"),
  updateEntities: require("./update-entities.js"),
  emptySpaces: require("./empty-spaces.js")
};
