const actions = ["eat", "fatigue", "grow", "randomMove", "linearMove"];
var mapLegend, mapSize, entities;

module.exports = {
  entities() {return entities;},
  size() {return mapSize;},
  setConfig(mapLayout, legend) {
    if (legend) {mapLegend = legend;}
    entities = module.exports.parse(mapLayout, mapLegend);
    return entities;
  },
  parse(map, legend) {
    var entities = [];

    if (!map || !legend) {
      throw new Error("map configuration should contain a legend and map");
    }

    if (!(map instanceof Array)) {
      throw new Error("invalid map - should be an array of rows");
    }

    mapSize = [map.length, 0];

    map.forEach((rowArray, rowIdx)=>{
      if (!(rowArray instanceof Array)) {
        throw new Error("invalid map - map array should contain array rows");
      }

      mapSize[1] = Math.max(mapSize[1], rowArray.length);

      rowArray.forEach((squareSymbol, colIdx)=>{
        if (squareSymbol === " ") {return;}

        if (typeof squareSymbol !== "string" || squareSymbol.length !== 1) {
          throw new Error("invalid map - rows array should contain characters");
        }

        if (!legend.hasOwnProperty(squareSymbol)) {
          throw new Error("invalid map - legend does not define all entities");
        }

        (legend[squareSymbol].actions) || (legend[squareSymbol].actions = []);

        legend[squareSymbol].actions.forEach((action)=>{
          if (actions.indexOf(action) === -1) {
            throw new Error("invalid action - " + action);
          }
        });

        entities.push(Object.assign({}, legend[squareSymbol], {posn: [rowIdx, colIdx]}));
      });
    });

    return entities;
  },
  useDefaultConfig() {
    return module.exports.setConfig([
      "############".split(""),
      "#          #".split(""),
      "## #       #".split(""),
      "#    #     #".split(""),
      "##O        #".split(""),
      "#     I    #".split(""),
      "#          #".split(""),
      "#O         #".split(""),
      "#          #".split(""),
      "#     B    #".split(""),
      "#          #".split(""),
      "############".split(""),
    ],
    {
      "O": {
        "type": "randomWalker",
        "energy": 100,
        "inertia": 3,
        "meat": true,
        "actions": ["randomMove", "fatigue", "eat"]
      },
      "B": {
        "type": "berries",
        "energy": 50,
        "edible": true,
        "growth": 2,
        "actions": ["grow"]
      },
      "#": {
        "type": "tree",
        "actions": []
      },
      "I": {
        "type": "linearBouncer",
        "energy": 100,
        "inertia": 1,
        "meat": true,
        "carnivore": true,
        "actions": ["linearMove", "fatigue", "eat"]
      }
    });
  }
};
