const utils = require("../utils");

module.exports = function grow([entities, idx]) {
  var energy = Math.min(100, entities[idx].energy + entities[idx].growth);

  if (entities[idx].energy <= 0) {
    return [entities, idx];
  }

  return [utils.updateEntities(entities, [
      {idx: idx, change: {energy: energy}}
  ]), idx];
};
