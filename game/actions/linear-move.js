const utils = require("../utils"),
randomMove = require("./random-move.js");

module.exports = function linearMove([entities, idx]) {
  if (entities[idx].energy <= 0) {
    return [entities, idx];
  }

  var validPosns, randomPosn, vectorPosn, result;

  if (!entities[idx].prevPosn) {
    result = randomMove([entities, idx])[0];
    result = utils.updateEntities(result, [
        {idx: idx, change: {prevPosn: Array.from(entities[0].posn)}}
    ]);
    return [result, idx];
  }

  validPosns = utils.emptySpaces(entities, idx);

  if (!validPosns.length) {return [entities, idx];}

  var vector = [
    entities[idx].posn[0] - entities[idx].prevPosn[0],
    entities[idx].posn[1] - entities[idx].prevPosn[1]
  ];

  vectorPosn = [entities[idx].posn[0] + vector[0], entities[idx].posn[1] + vector[1]];

  if (!validPosns.some((validPosn)=>{
    return validPosn[0] === vectorPosn[0] && validPosn[1] === vectorPosn[1];
  })) {
    randomPosn = validPosns[Math.floor(Math.random() * validPosns.length)];
    vectorPosn = randomPosn;
  }

  return [utils.updateEntities(entities, [
    {idx: idx, change: {posn: vectorPosn, prevPosn: entities[idx].posn}}
  ]), idx];
};
