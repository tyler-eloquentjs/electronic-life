const utils = require("../utils");

module.exports = function fatigue([entities, idx]) {
  if (entities[idx].energy <= 0) {
    return [entities, idx];
  }

  return [utils.updateEntities(entities, [
    {idx: idx, change: {energy: entities[idx].energy - entities[idx].inertia}}
  ]), idx];
};
