const utils = require("../utils");

module.exports = function randomMove([entities, idx]) {
  if (entities[idx].energy <= 0) {
    return [entities, idx];
  }

  var validPosns = utils.emptySpaces(entities, idx);

  if (!validPosns.length) {
    return [entities, idx];
  }

  randomPosn = validPosns[Math.floor(Math.random() * validPosns.length)];

  return [utils.updateEntities(entities, [
    {idx: idx, change: {posn: randomPosn}}
  ]), idx];
};
