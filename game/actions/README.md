## Actions

Action functions are applied sequentially to each entity on every turn.  

Each action is of the form ([entities, idx]) => [entities, idx]

That is, each action function receives an array consisting of two things:

 1. an array of entity objects
 2. an integer index indicating the primary entity being acted upon

The output of the action function is an updated array of entity objects and the same index. This way, action functions can be called one after the other, as the output from one becomes the input to the next.
