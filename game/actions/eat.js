const utils = require("../utils");

module.exports = function eat([entities, idx]) {
  if (entities[idx].energy <= 0) {
    return [entities, idx];
  }

  var transfer, victimIndex, sourcePosn = entities[idx].posn,
  victim = entities.find((target, targetIdx)=>{
    var vector = [sourcePosn[0] - target.posn[0], sourcePosn[1] - target.posn[1]];

    victimIndex = targetIdx;

    return ((target.meat && entities[idx].carnivore) || target.edible) &&
    target.energy > 0 &&
    Math.abs(vector[0]) < 2 &&
    Math.abs(vector[1]) < 2 &&
    (!(sourcePosn[0] === target.posn[0] && sourcePosn[1] === target.posn[1]));
  });

  if (!victim) {return [entities, idx];}
  transfer = Math.min(100 - entities[idx].energy, victim.energy);

  return [utils.updateEntities(entities, [
    {idx: idx, change: {energy: entities[idx].energy + transfer}},
    {idx: victimIndex, change: {energy: victim.energy - transfer}} 
  ]), idx];
};
