# Electronic Life

This is an interpretation of the 7th chapter project from [Eloquent JavaScript](http://eloquentjavascript.net), done in a functional programming style.  

## Run unit tests:
```js
npm test
```

## Two output options:
```js
npm run console
npm run browser (expects chromium - can be modified in package.json)
```
